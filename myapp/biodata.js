import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  textforgot,
  Button,
  TouchableOpacity,
} from "react-native";
 
export default function App() {
  const [Nama, setnama] = useState("");
  const [kelas, setkelas] = useState("");
  const [programstudi, setprogramstudi] = useState("");

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Image style={styles.image} source={require('./rini.jpeg')} />
      <Text style={styles.text}>Welcome Rini Afriani!
      </Text>
      <Text style={styles.text2}>Mahasiswa stt wastukancana
      </Text>
      <Text style={styles.text3}>Nama :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Rini Afriani"
          placeholderTextColor="#000"
          onChangeText={(nama) => setnama(nama)}
        />
      </View>
     <Text style={styles.text4}>Kelas :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Malam C"
          placeholderTextColor="#000"
          secureTextEntry={true}
          onChangeText={(kelas) => setkelas(kelas)}
        />
      </View>
      <Text style={styles.text5}>Program Studi :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Teknik Informatika"
          placeholderTextColor="#000"
          secureTextEntry={true}
          onChangeText={(programstudi) => setprogramstudi(programsutdi)}
        />
      </View>
      <Text style={styles.text5}>Akun Sosmed :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="@aapr.l"
          placeholderTextColor="#000"
          secureTextEntry={true}
          onChangeText={(programstudi) => setprogramstudi(programsutdi)}
        />
      </View>
    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 100,
    backgroundColor: "#9FF8A8",
    alignItems: "center",
    justifyContent: "center",
  },
  
  text: {
    width : 250,
    marginTop: 20,
    textAlign : "left",
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
    
      },

 text2: {
    width : 250,
    fontSize : 12,
    fontWeight : "normal",
    color : "00000",
    marginBottom: 20,
    
      },
  text3: {
    width : 250,
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
      },
    text4: {
    width : 250,
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
      },
 text5: {
    width : 250,
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
      },
  image: {
    borderRadius: 100,
    width: 100,
    height: 100,
    right: 80,
    
  },
 
  inputView: {
    backgroundColor: "#fff",
    borderRadius: 5,
    width: "70%",
    height: 35,
    marginBottom: 20,
   right : 15,
  },
 
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 5,
  },
 
});
