import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  textforgot,
  Button,
  TouchableOpacity,
} from "react-native";
 
export default function App() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
 
  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
        <Text style={styles.text}>Welcome!
</Text>
<Text style={styles.text1}>please log in your account to portofolio.
</Text>
            <Image style={styles.image} source={require('/login.svg')} />
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Username"
          placeholderTextColor="#ABA9A9"
          onChangeText={(email) => setEmail(email)}
        />
      </View>
 
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Password"
          placeholderTextColor="#ABA9A9"
          secureTextEntry={true}
          onChangeText={(password) => setPassword(password)}
        />
      </View>
      <TouchableOpacity style={styles.textforgot}>
      <Text style={styles.textforogt}>forgot password
      </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.loginBtn}>
        <Text style={styles.loginText}>Login</Text>
      </TouchableOpacity>

    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 100,
    backgroundColor: "#9FF8A8",
    alignItems: "center",
    justifyContent: "center",
  },
  
  text: {
    fontSize:18,
    width:250,
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
    
      },

    text1: {
      width:250,
    fontSize:10,
    marginBottom: 0,
    
      },
 
  image: { 
    marginTop : 20,
    width: 150,
    height: 150,
    left: 40,
    marginBottom: 20,
  },
 
  inputView: {
    backgroundColor: "#fff",
    borderRadius: 25,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "left",
  },
 
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 5,
  },
 
  forgot_button: {
    height: 10,
    marginBottom: 30,
  },

  textforgot: {
    fontWeight : "bold",
    fontFamily : "quicksand",
    height: 10,
    marginBottom: 10,
  },
 
  loginBtn: {
    width: "50%",
    borderRadius: 20,
    height: 45,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 25,
    backgroundColor: "#fff",
  },

  reg_button: {
    height: 40,
    marginTop: 20,
    marginBottom: 10,
  },
});
